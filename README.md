string
======
* member: string, text
* operator
  > contains
    name-contains="ini", username-contains="nug hasan"
    DB::where('name', 'like', $_get);
  > prefix
    name-prefix="nu"

numeric
=======
* member: integer, decimal, float
* operator
  > equal
    amount-equal="30"
  > min
    amount-min="10"
  > max
    amount-max="20"

date/datetime/time
==================
* member: date, datetime, time
* operator
  > at
  > after
  > before

boolean
* operator
  > is

list
* member: list, relation
* operator
  > in

BUG
===

AdvanceGridBuilder
------------------
- Method `filter` bila dipanggil setelah `with` tidak bisa, krn `applyFilter` di panggil dalam `with`
- Filtering pke _GET dipisah dr AdvanceGrid aja pa ya!
<table class="table table-hover table-aql">
  <thead>
    <tr>
      <!-- <th class="selector">&nbsp;</th> -->
      @foreach ($columns as $column)
      <th><a href="#"><i class="icon-sort-down pull-right"></i> {{ $column['title'] }}</a></th>
      @endforeach
      <th class="actions">&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    @foreach($rows as $row)
    <tr>
      <!-- <td class="selector"><input type="checkbox"></td> -->
      @foreach($columns as $column)
      <td>
        <?php echo (isset($column['value'])) ? $column['value']($row) : $row->{$column['name']} ?>
      </td>
      @endforeach
      <td class="actions">
        <div class="btn-group">
          <button class="btn btn-small"><i class="icon-pencil"></i> Edit</button>
          <button class="btn btn-small dropdown-toggle" data-toggle="dropdown">
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu pull-right">
            <li><a href="#"><i class="icon-retweet"></i> Toggle</a></li>
            <li><a href="#"><i class="icon-trash"></i> Delete</a></li>
          </ul>
        </div>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
<?php
namespace Masnug\Support\Table;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

/**
* TableModelBuilder
*/
class TableModelBuilder extends TableBuilder
{

    protected $model;
    protected $paginate;
    protected $pagination;

    public function __construct()
    {
        if (method_exists($this, 'init')) {
            $this->init();
        }
    }

    public function with($model, $paginate = true)
    {
        $this->model    = $model;
        $this->paginate = $paginate;

        if ($paginate) {
            if ( ! $model instanceof Paginator) {
                $this->model = $model->paginate();
            }
            $this->pagination = $this->model->links();
            $rows = $this->model->getItems();
        }
        else {
            $rows = $this->model;
        }

        if ($rows instanceof Collection)
        {
            $rows = $rows->all();
        }

        $this->rows($rows);

        return $this;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getPagination()
    {
        return $this->paginate ? $this->pagination : null;
    }

    public function render()
    {
        return View::make('support::table.model')
            ->with('table', $this)
            ->render();
    }
}
<?php
namespace Masnug\Support\Table;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

/**
* TableBuilder
*/
class TableBuilder
{

    protected $columns = array();
    protected $model;
    protected $rows;
    protected $paginate;
    protected $pagination;

    public function __construct()
    {
        if (method_exists($this, 'init')) {
            $this->init();
        }
    }

    public function rows(array $rows = null)
    {
        $this->rows = $rows;

        return $this;
    }

    public function getRows()
    {
        return new Rows($this->rows, $this->columns);
    }

    public function column($key, $title = null, $yield = null)
    {
        if (! isset($this->columns[$key])) {
            $this->columns[$key] = new Column($key, $title);
        }

        if (is_callable($yield)) {
            $this->columns[$key]->yield($yield);
        }

        return $this->columns[$key];
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function render()
    {
        return View::make('support::table.base')
            ->with('table', $this)
            ->render();
    }

    public function __toString()
    {
        return $this->render();
    }

    public function __get($key)
    {
        $method_name = Str::camel('get_'.$key);
        if (method_exists($this, $method_name)) {
            return $this->{$method_name}();
        }
        elseif (array_key_exists($key, $this->columns)) {
            return $this->columns[$key];
        }

        return null;
    }

    public function __isset($key)
    {
        $method_name = Str::camel('get_'.$key);
        return method_exists($this, $method_name)
            || array_key_exists($key, $this->columns);
    }
}
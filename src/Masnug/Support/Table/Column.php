<?php
namespace Masnug\Support\Table;

use Illuminate\Support\Str;

/**
* Column
*/
class Column
{

    protected $key;
    protected $title;
    protected $data;
    protected $yield;

    public function __construct($key, $title = null, $yield = null, $data = null)
    {
        $this->key = $key;
        $this->title = $title;
        $this->yield = $yield;
        $this->data = $data;
    }

    public function data($data)
    {
        $this->data = $data;
        return $this;
    }

    public function title($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getTitle()
    {
        return $this->title ?: $this->key;
    }

    public function yield($yield)
    {
        $this->yield = $yield;
        return $this;
    }

    public function getYield()
    {
        $data = (object)$this->data;
        // dd($data);
        if (empty($this->yield)) {
            return $data->{$this->key};
        }
        elseif (is_string($this->yield)) {
            return $data->{$this->yield};
        }
        elseif (is_callable($this->yield)) {
            $fn = $this->yield;
            return $fn($data);
        }

        return null;
    }

    public function getValue()
    {
        return $this->getYield();
    }

    public function __toString()
    {
        return $this->title;
    }

    public function __get($key)
    {
        $method_name = Str::camel('get_'.$key);
        if (method_exists($this, $method_name)) {
            return $this->{$method_name}();
        }

        return null;
    }

    public function __isset($key)
    {
        $method_name = Str::camel('get_'.$key);
        return method_exists($this, $method_name);
    }
}
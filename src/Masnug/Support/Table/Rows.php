<?php
namespace Masnug\Support\Table;

use Iterator;
use Illuminate\Support\Str;

/**
* Rows
*/
class Rows implements Iterator
{

    private $compiled = false;
    private $data = array();
    protected $columns = array();

    public function __construct($array = array(), $columns = array())
    {
        $this->columns = $columns;
        $this->data = $array;
    }

    public function rewind()
    {
        $this->compiled = false;
        reset($this->data);
    }

    public function current()
    {
        $data = current($this->data);

        $row['_data'] = $data;
        foreach ($this->columns as $key => $column) {
            $row['columns'][$key] = $column->data($data);
        }

        return $row;
    }

    public function key()
    {
        $data = key($this->data);
        return $data;
    }

    public function next()
    {
        $this->compiled = false;
        $data = next($this->data);
        return $data;
    }

    public function valid()
    {
        $key = key($this->data);
        $data = ($key !== NULL && $key !== FALSE);
        return $data;
    }

}
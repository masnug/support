<?php
namespace Masnug\Support\Form;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

/**
* Fieldset
*/
class Fieldset
{

    protected $key;
    protected $legend;
    protected $controls;

    public function __construct($key, $legend)
    {
        $this->key = $key;
        $this->legend = $legend;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getLegend()
    {
        return $this->legend;
    }

    public function control($name)
    {
        if (! isset($this->controls[$name]))
        {
            $this->controls[$name] = new Grid($name);
        }

        return $this->controls[$name];
    }

    public function controls($name)
    {
        $name($this);
    }

    public function getControls()
    {
        return $this->controls;
    }

    public function hasControl($name)
    {
        return array_key_exists($name, $this->controls);
    }

    public function render()
    {
        return View::make('support::form.fieldset')
            ->with('fieldset', $this)->render();
    }

    public function __toString()
    {
        return $this->render();
    }

    public function __get($key) {
        $method_name = Str::camel('get' . $key);
        if (method_exists($this, $method_name)) {
            return $this->$method_name();
        }
        elseif (array_key_exists($key, $this->controls)) {
            return $this->controls[$key];
        }

        return null;
    }

    public function __isset($key) {
        $method_name = Str::camel('get' . $key);
        return isset($this->controls[$key]) || method_exists($this, $method_name);
    }

    public function __unset($key) {
        unset($this->controls[$key]);
    }
}
<?php
namespace Masnug\Support\Form;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Form;
use Illuminate\Support\Str;

/**
* Grid
*/
class Grid
{

    protected $name;
    protected $label_text;
    protected $widget;
    protected $rules;
    protected $value;
    private $form;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function label($label_text)
    {
        $this->label_text = $label_text;

        return $this;
    }

    public function getLabelText()
    {
        return $this->label_text ?: $this->name;
    }

    public function getLabel($label_text = null, $options = array())
    {
        $options = array_merge(array('class' => 'control-label'), $options);
        $label_text = $label_text ?: $this->getLabelText();

        if (in_array('required', (array)$this->rules)) {
            $label_text .= '*';
        }

        return Form::label($this->name, $label_text, $options);
    }

    public function widget($widget)
    {
        $this->widget = $widget;
        $this->widgetPreCompile();

        return $this;
    }

    /**
     * Untuk mendapatkan css dari widget macro
     * cara kotor nih :(
     */
    public function widgetPreCompile()
    {
        $this->getWidget();
    }

    public function getWidget($value = null, $options = array())
    {
        $value = $value ?: $this->value;
        if (is_callable($this->widget)) {
            $fn = $this->widget;
            return $fn($value);
        }
        else {
            return Form::text($this->name, $value, $options);
        }
    }

    public function rules($rules)
    {
        if (! is_array($rules)) {
            $rules = explode('|', $rules);
            array_walk($rules, 'trim');
        }
        $this->rules = $rules;

        return $this;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function value($value = null)
    {
        $this->value = $value;

        return $this;
    }

    public function render()
    {
        return View::make('support::form.control')
            ->with('control', $this)->render();
    }

    public function __toString()
    {
        return $this->render();
    }

    public function __get($key) {
        $method_name = 'get' . Str::studly($key);
        if (method_exists($this, $method_name)) {
            return $this->$method_name();
        }

        return null;
    }

    public function __isset($key) {
        $method_name = 'get' . Str::studly($key);
        return method_exists($this, $method_name);
    }
}
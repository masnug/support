<?php namespace Masnug\Support\Form;

use Illuminate\Support\Facades\Form;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

/**
* FormBuilder
*/
abstract class FormBuilder
{

    protected $model;
    protected $attributes = array();
    protected $fieldsets = array();
    protected $hiddens = array();

    public function __construct()
    {
        if (method_exists($this, 'init')){
            $this->init();
        }
    }

    /**
     * Sisipkan data ke form dalam
     * <code>
     *
     * </code>
     */
    public function with($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Create a scope scenario.
     *
     * @param  string   $scenario
     * @param  array    $parameters
     * @return self
     */
    public function on(/*$scenario, $parameters = array()*/)
    {
        $parameters = func_get_args();
        $scenario = array_shift($parameters);

        $method = 'on' . Str::studly($scenario);
        if (method_exists($this, $method))
        {
            call_user_func_array(array($this, $method), (array)$parameters);
        }

        return $this;
    }

    public function formAttributes($key, $value = null)
    {
        if (is_array($key)) {
            $this->attributes = array_merge($this->attributes, $key);
        } else {
            $this->attributes[$key] = $value;
        }

        return $this;
    }

    public function fieldset($key = 'default', $legend = null, $callback = null)
    {
        $legend = $legend ?: $key;
        if (! isset($this->fieldsets[$key])) {
            $this->fieldsets[$key] = new Fieldset($key, $legend);
        }

        if (is_callable($callback)) {
            $callback($this->fieldsets[$key]);
        }

        return $this->fieldsets[$key];
    }

    public function hasFieldset($key)
    {
        return array_key_exists($key, $this->fieldsets);
    }

    public function getFieldsets()
    {
        return $this->fieldsets;
    }

    public function hidden($name, $value = null, $options = array())
    {
        if (empty($name)) return;

        if (is_array($name)) {
            foreach ($name as $key => $value) {
                $this->hidden($key, $value);
            }
        }

        $this->hiddens[$name] = compact('name', 'value', 'options');
    }

    public function getHiddens()
    {
        return $this->hiddens;
    }

    public function open($attributes = array())
    {
        $attributes = array_merge($this->attributes, $attributes);
        if ($this->model) {
            return Form::model($this->model, $attributes);
        } else {
            return Form::open($attributes);
        }
    }

    public function close($hiddens = array())
    {
        $this->hidden($hiddens);

        $tag_hidden = '';
        if ($this->hiddens) {
            foreach ($this->hiddens as $name => $attrs) {
                $tag_hidden .= Form::hidden($name, $attrs['value'], $attrs['options']);
            }
        }
        return $tag_hidden . Form::close();
    }

    public function render()
    {
        return View::make('support::form.form')
            ->with('form', $this)->render();
    }

    public function getRules()
    {
        if (empty($this->fieldsets)) return null;

        $rules = array();
        foreach ($this->fieldsets as $key => $fieldset) {

            $controls = $fieldset->getControls();
            if (empty($controls)) continue;

            foreach ($controls as $name => $control) {
                $control_rules = $control->getRules();
                if (empty($control_rules)) continue;
                $rules[$name] = $control_rules;
            }
        }

        return $rules;
    }

    public function validator($input = null)
    {
        $rules = $this->getRules();
        // if (
        //     (empty($input) && empty($this->model))
        //     || empty($rules)
        // ) {
        //     return true;
        // }

        $input = $input ?: $this->model;

        return Validator::make((array)$input, $rules);
    }

    public function __toString()
    {
        return $this->render();
    }

    public function __get($key) {
        $method_name = Str::camel('get' . $key);
        if (method_exists($this, $method_name)) {
            return $this->$method_name();
        }
        elseif (array_key_exists($key, $this->fieldsets)) {
            return $this->fieldsets[$key];
        }
        elseif (false !== ($fieldset = $this->seekControl($key))) {
            return $this->fieldsets[$fieldset]->{$key};
        }

        return null;
    }

    public function __isset($key) {
        $method_name = Str::camel('get' . $key);

        return false !== $this->seekControl($key)
            || method_exists($this, $method_name)
            || isset($this->fieldsets[$key]);
    }

    public function __unset($key) {
        if ($key == '_fieldsets') {
            $this->fieldsets = null;
        }
        elseif (false !== ($fieldset = $this->seekControl($key))) {
            unset($this->fieldsets[$fieldset]->{$key});
        }
        else {
            unset($this->fieldsets[$key]);
        }
    }

    private function seekControl($name)
    {
        foreach ($this->fieldsets as $key => $fieldset) {
            if ($fieldset->hasControl($name)) return $key;
        }

        return false;
    }
}
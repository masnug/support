<?php namespace Masnug\Support;

use Illuminate\Support\Facades\HTML;
use Illuminate\Support\Facades\Form;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\Paginator;

/**
* DataGrid
*/
abstract class DataGrid
{
    
    protected $columns;
    protected $model;
    protected $rows;
    protected $paginate;
    protected $actions;

    public function __construct()
    {
        $this->init();
    }

    protected function init()
    {
        $this->setColumns($this->columns);
    }

    public function with($model, $paginate = true)
    {
        $this->model    = $model;
        $this->paginate = $paginate;
        if ($paginate and ( ! $model instanceof Paginator))
        {
            $this->model = $model->paginate();
        }

        $rows = (true === $paginate) ? $this->model->getItems() : $this->model;
        if ($rows instanceof Collection)
        {
            $rows = $rows->all();
        }
        // dd($rows);
        $this->setRows($rows);

        return $this;
    }

    public function setRows(array $rows = null)
    {
        if (is_null($rows)) return $this->rows;

        $this->rows = $rows;
        
        return $this;
    }

    public function setColumns(array $columns = null)
    {
        if (is_null($columns)) return $this->columns;

        $_columns = array();
        foreach ($columns as $col) {
            if (! isset($col['name']))
            {
                throw new Exception('Kolom harus memiliki elemen setidaknya "name"', 1);
            }

            $col['title'] = isset($col['title']) ? $col['title'] : $col['name'];
            $col['value'] = isset($col['value']) ? $col['value']
                : function($row) use ($col) { return $row->{$col['name']}; };

            $_columns[] = $col;
        }
        $this->columns = $_columns;
        
        return $this;
    }

    public function setRowActions($url, $label = null, $collapse = false)
    {
        if (is_array($url))
        {
            foreach ($url as $action) {
                $this->setRowActions(
                    $action['url'],
                    $action['label'],
                    (isset($action['collapse'])) ? $action['collapse'] : false
                    );
            }
        }
        else
        {
            $_url = function($row) use ($url) {
                if (! preg_match_all('~\:([a-z][\w-_]+)~', $url, $m))
                {
                    return $url;
                }
                foreach ($m[1] as $i => $key) {
                    $search[$i] = '~'.$m[0][$i].'~';
                    $replacement[$i] = $row->{$key};
                }
                return preg_replace($search, $replacement, $url);
            };
            $_label = function($row) use ($label) {
                if (! preg_match_all('~\:([a-z][\w-_]+)~', $label, $m))
                {
                    return $label;
                }

                foreach ($m[1] as $key) {
                    $search[$i] = '~'.$m[0][$i].'~';
                    $replacement[] = $row->{$key};
                }
                return preg_replace($search, $replacement, $label);
            };

            $group = $collapse ? 'dropdown' : 'button';
            $this->actions[$group][] = array(
                'url' => $_url,
                'label' => $_label
                );
        }

        return $this;
    }

    public function render()
    {
        return View::make('components.grid')
            ->with('columns', $this->columns)
            ->with('rows', $this->rows)
            ->with('actions', $this->actions)
            ->render();
    }

    public function __toString()
    {
        return $this->render();
    }
    
}
<?php
namespace Masnug\Support\AdvanceGrid;

use Input;
use Masnug\Support\Table\TableBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

/**
* TableModelBuilder
*/
class AdvanceGridBuilder
{

    protected $table;
    protected $model;
    protected $paginate;
    protected $pagination;
    protected $filters;

    public function __construct()
    {
        $this->table = new TableBuilder;

        if (method_exists($this, 'init')) {
            $this->init();
        }
    }

    public function table($callback)
    {
        return $callback($this->table);
    }

    public function getTable()
    {
        return $this->table;
    }

    public function with($model, $paginate = true)
    {
        $this->model    = $model;
        $this->paginate = $paginate;

        if ($paginate) {
            if ( ! $model instanceof Paginator) {
                $this->model = $model->paginate();
            }
            $this->pagination = $this->model->appends(Input::except('page'))->links();
            $rows = $this->model->getItems();
        }
        else {
            $rows = $this->model;
        }

        if ($rows instanceof Collection)
        {
            $rows = $rows->all();
        }

        $this->table->rows($rows);

        return $this;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getPagination()
    {
        return $this->paginate ? $this->pagination : null;
    }

    public function render()
    {
        dd($this->order_statements);
        return View::make('support::advance_grid.grid')
            ->with('grid', $this)
            ->render();
    }

    public function __toString()
    {
        return $this->render();
    }

    public function __get($key)
    {
        $method_name = Str::camel('get_'.$key);
        if (method_exists($this, $method_name)) {
            return $this->{$method_name}();
        }

        return null;
    }

    public function __isset($key)
    {
        $method_name = Str::camel('get_'.$key);
        return method_exists($this, $method_name);
    }
}